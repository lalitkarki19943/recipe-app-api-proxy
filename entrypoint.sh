#!/bin/sh

#if script fails, error will be returned to the screen
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
#tells nginx to not run in the background, it is needed for docker as logs will be printed in the docker output
nginx -g 'daemon off;'